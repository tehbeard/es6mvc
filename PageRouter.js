"use strict";
var Router = require('./Router');
var View = require('./View');

module.exports = class PageRouter extends Router {
  constructor(opts){
    super(opts);
    if(!opts.region){throw new Error("Must supply a region");}
    this.region = opts.region instanceof HTMLElement ? opts.region : document.querySelector(opts.region);
    if(!this.region){throw new Error("Region not found, called before page load?");}
    this.currentView = null;
  }

  add(route, page){
    if(page instanceof Router){
      super.add(route,page);
      return this;
    }
    if(
        !(page instanceof View) &&
        !(page instanceof Promise) &&
        !(typeof page != 'function')
      ){
      throw new Error('Must supply a View, Promise or function.');
    }
    var pageOrPromise = (page instanceof View || page instanceof Promise) ? page : null;
    super.add(route, (ctx, next) => {
      //Lazy load page first time
      if(pageOrPromise == null){
        pageOrPromise = page();
      }
      Promise.resolve(pageOrPromise).then((pageInst)=>{
        //Unload current page
        if(this.currentView){
          this.currentView.emit("unload");
          this.region.removeChild(this.currentView.el);
          this.currentView = null;
        }
        //emit load to the page so it can re-render if needed
        //Passed context, done() (render to DOM), and next() to continue 
        //Processing PageRouter
        pageInst.emit("load",ctx, () => {
          this.currentView = pageInst;
          this.region.appendChild(this.currentView.el);
        },next);
      })
    });
    return this;
  }

  use(route, fn){
      super.add(route,fn);
    }
}