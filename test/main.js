"use strict";
var PageRouter = require('../PageRouter');
var Model = require('../Model');
var View = require('../View');
var Collection = require('../Collection');

    class TODOItem extends Model {

        parse(attr){
            if( !(attr.due instanceof Date)){
                attr.due = new Date(attr.due);
            }
            return attr;
        }

        isOverdue(){
            return (Date.now() - this.get('due').getTime()) > 0;
        }
    }

    class TODOList extends Collection {
        constructor(models){
            super(models,{model: TODOList});
        }
    }

    class TODOView extends View {
        constructor(collection){
            super({
                events:{
                    'click #add-todo': () => { this.collection.add(new TODOItem({task:this.qs('[name=task]').value,due: this.qs('[name=due]').value}))}
                }
            });
            this.collection = collection;
            this.el.innerHTML = `<div id='todos'></div>
<div id='add-todo-form'>
<label for='task'>Task: <input name='task'/></label>
<label for='due'>Due: <input name='due' type='date'/></label>
<button id='add-todo'>Add</button>
</div>`;
            this.on('load',(ctx, done, next)=>{console.log('loading',ctx,done,next);this.render();done();})
            this.collection.on('change',()=>this.render());
        }

        render() {
             this.qs('#todos').innerHTML = this.collection.getAll().map((e) => `<div class="todo">
<p><strong>Task</strong> ${e.get('task')}</p>
<p><strong>Due</strong> ${e.get('due').toLocaleString()}</p>
<p><strong>Done?</strong> ${e.get('done') ? 'yes':'no'}</p>
<p><strong>Overdue?</strong> ${e.isOverdue() ? 'yes':'no'}</p>
</div>`).reduce((a,b) => a+b)
        }
    }

    var col = new TODOList();

    col.add(new TODOItem({task:"Eat birthday cake!", due:"2013-10-30"}))
    col.add(new TODOItem({task:"Get Drunk!", due:"2015-10-30"}))

    var router = new PageRouter({region:'#main-app'});
    
    router.add('/', new TODOView(col));
    router.start(true);
